import { Injectable } from '@angular/core';
import { Equipamento } from '../interfaces/equipamento';
import { DataService} from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class EquipamentoService {

  public equiplist: Equipamento [];

  constructor(private data: DataService) {
    this.equiplist = data.get();
   }
   
   save(equip: Equipamento[]){
     this.data.set(equip);
   }
}
