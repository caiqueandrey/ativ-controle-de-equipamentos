import { Injectable } from '@angular/core';
import { Equipamento } from '../interfaces/equipamento';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  public get(){
    const aux = localStorage.getItem('data');
    if(aux){
      return JSON.parse(aux);
    }
    return [];
  }
  
  set (equip: Equipamento[]){
    const aux = JSON.stringify(equip);
    localStorage.setItem('data',aux);
  }
}
