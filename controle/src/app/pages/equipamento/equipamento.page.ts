import { Component, OnInit } from '@angular/core';
import { AlertController, NavController} from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Equipamento } from 'src/app/interfaces/equipamento';
import { EquipamentoService } from 'src/app/services/equipamento.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-equipamento',
  templateUrl: './equipamento.page.html',
  styleUrls: ['./equipamento.page.scss'],
})
export class EquipamentoPage implements OnInit {
  equip: string [];
  lista: Equipamento[];

  constructor(
    private http: HttpClient,
    private alert: AlertController,
    public navCtrl: NavController,
    private route: ActivatedRoute,
    private service: EquipamentoService
    
  ) {  
   }

  ngOnInit() {
    this.lista= this.service.equiplist;
  }
  ionViewWillEnter(){
    const email = this.route.snapshot.paramMap.get('email'); 
    //this.verifyEmail(email);
    console.log(email);
  }
  private save(data){
    const equip: Equipamento = {nome: data.nome, status: data.status};
    this.lista.push(equip);
    this.service.save(this.lista);
  }

  async responsavel(equipamento: Equipamento){
    const form = await this.alert.create({
      header: 'Empréstimo',
      message: 'Altere o nome da tarefa',
      inputs: [{type: 'text', name: 'equip', value: equipamento.nome}],
      buttons: [
        {text: 'Cancelar'},
        {text: 'Pedir Empréstimo', handler: data => this.edita(equipamento, data)}
      ]
    });

    form.present();
  }

  private edita(equipamento: Equipamento, data){
    equipamento.nome = data.equip;
    this.service.save(this.lista);
  }

}
