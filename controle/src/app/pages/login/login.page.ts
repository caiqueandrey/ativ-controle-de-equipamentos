import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { AlertController} from '@ionic/angular';
import { EquipamentoPage } from '../equipamento/equipamento.page';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  db: any [];
  email:string;
  senha: string;
  formValid = false;
  navCtrl: any[] = [];
  equipamentoPage = EquipamentoPage;

  constructor(
    private http: HttpClient,
    public nav: NavController,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.http.get('assets/config/usuarios.json').
    subscribe((resp:any)=> this.db = resp.data);
  }

  valida(text: string){
    if(text.length < 8){
      this.formValid = false;
    } else{
      this.formValid = true;
    }
  }

  login(){
    const obj = this.db.filter(x => x.email === this.email)[0];
    if(typeof obj !== 'undefined' && obj.senha === this.senha){
      this.nav.navigateForward(['/equipamento',this.email]);
    }else{
        this.erroLogin();
    }
  }

  async erroLogin(){
    const alerta = await this.alert.create({
      header: 'Erro ao logar',
      message: 'Dados incorretos, digite novamente!',
      buttons:['OK'],
      cssClass: 'alertLogin' 
    })
    alerta.present();
  }

}
